/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;

/**
 *
 * @author 
 */
public class MakeCSV {

    DatabaseConnection dc = new DatabaseConnection();
    Connection con;
    Statement stmt;
    String query;

    public MakeCSV(String qu) {
        query = qu;
        generateCsvFile(".\\database\\test.csv");
    }

    private void generateCsvFile(String sFileName) {
        try {
            FileWriter writer = new FileWriter(sFileName);
            try {
                con = dc.database_connection_main();
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet rs = stmt.executeQuery(query);

                String cell = "";
                String c, c1;
                int i = 0;
                rs.last();
                i = rs.getRow();
                rs.beforeFirst();
                if (i <= 0) {
                    javax.swing.JOptionPane.showMessageDialog(null, "No row selected.");
                } else {
                    i = 1;

                    String status = "Pending";

                    writer.append("Cell No");
                    writer.append(',');
                    writer.append("Roll No");
                    writer.append(',');
                    writer.append("Message");
                    writer.append(',');
                    writer.append("Status");
                    writer.append(',');
                    writer.append("User");
                    writer.append(',');
                    writer.append("Semester");
                    writer.append(',');
                    writer.append("Division");
                    writer.append(',');
                    writer.append("Branch");
                    writer.append(',');
                    writer.append("Sent Date");
                    writer.append(',');
                    writer.append("Sent Time");
                    writer.append('\n');
                    while (rs.next()) {
                        if (rs.getInt("status") == 1) {
                            status = "Sent";
                        } else if (rs.getInt("status") == 2) {
                            status = "Failed";
                        }
                        writer.append(rs.getString("cell_no"));
                        writer.append(',');
                        writer.append(rs.getString("roll_no"));
                        writer.append(',');
                        writer.append(rs.getString("message"));
                        writer.append(',');
                        writer.append(status);
                        writer.append(',');
                        writer.append(rs.getString("user"));
                        writer.append(',');
                        writer.append(rs.getString("semester"));
                        writer.append(',');
                        writer.append(rs.getString("division"));
                        writer.append(',');
                        writer.append(rs.getString("branch"));
                        writer.append(',');
                        writer.append(rs.getString("sent_date"));
                        writer.append(',');
                        writer.append(rs.getString("sent_time"));
                        writer.append('\n');
                        i = i + 1;
                    }
                    con.close();
                    writer.flush();
                    writer.close();
                    try {
                        //Path for folder or file that you want to open
                        //In my case i will open C:\ drive
                        //So create a String like below
                        //Don't forget to change \ to \\

                        //Execute command
                        //Command that you must know : rundll32 SHELL32.DLL,ShellExec_RunDLL File_Or_Folder_To_Open
                        Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL excel \"" + sFileName + "\"");
                    } catch (Exception ex) {
                        javax.swing.JOptionPane.showMessageDialog(null, ex);
                    }
                }
            } catch (SQLException ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }
            //generate whatever data you want


        } catch (IOException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
    }
}
