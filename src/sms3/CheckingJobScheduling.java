/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimerTask;
import java.sql.*;
import java.text.DateFormat;
import java.util.regex.Pattern;

/**
 *
 * @author 
 */
public class CheckingJobScheduling extends TimerTask {

    /**
     * Class for checking any job available for scheduling or not
     * if any job available then generate message for that job and put all messages in the queue table for sending with priority 1 and status 0
     * status 0 for pending for sending
     */
    /**Creating database connection class instance*/
    DatabaseConnection dc = new DatabaseConnection();
    /**Creating connection instance*/
    Connection con;
    private final int timerInterval;
    /**creating engine class object*/
    Engine engine;

    /**
     * constructor for initializing engine object and time interval for scheduling
     */
    public CheckingJobScheduling(int timeInterval, Engine en) {

        this.timerInterval = timeInterval;
        engine = en;
    }

    public static String now(String dateFormat) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());

    }

    @Override
    public void run() {

        int job_id, schedule_id;
        Date date = null;
        java.util.Date date1 = null, time1 = null;
        Time time = null;
        String type = "";

        try {
            /**initializing connection object*/
            con = dc.database_connection_main();

            Statement stmt = con.createStatement();
            /**query for getting all job schedule*/
            String q = "SELECT * from schedule where status=1";
            ResultSet rs = stmt.executeQuery(q);

            while (rs.next()) {
                /**getting time,date,repeat type,job id and schedule id*/
                date = rs.getDate("sdate");
                time = rs.getTime("stime");
                type = rs.getString("repeat_type");
                schedule_id = rs.getInt("schedule_id");
                job_id = rs.getInt("job_id");
                String d1 = CheckingJobScheduling.now("yyyy-MM-dd");
                String t1 = CheckingJobScheduling.now("H:mm:ss");
                DateFormat formatter;
                formatter = new SimpleDateFormat("yyyy-MM-dd");
                try {
                    date1 = (java.util.Date) formatter.parse(d1);
                } catch (ParseException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                /**Comparing date with current date
                 *if less,add job to queue
                 */
                if (date.compareTo(date1) <= 0) {

                    formatter = new SimpleDateFormat("H:mm:ss");
                    try {
                        time1 = (java.util.Date) formatter.parse(t1);
                    } catch (ParseException ex) {
                        javax.swing.JOptionPane.showMessageDialog(null, ex);
                    }
                    sdf = new SimpleDateFormat("H:mm:ss");

                    if (time.compareTo(time1) >= 0) {
                        /**Calling function put_job_for_scheduling for adding messages and cell numbers to queue*/
                        put_job_for_scheduling(job_id, date, time, type, schedule_id);
                    }
                }
            }
            con.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, "Sdfsd" + ex);
        }
    }

    /**
     * function for generating messages and put them in queue and also schedule job according repeat type of job
     */
    public void put_job_for_scheduling(int job_id, Date date, Time time, String type, int schedule_id) {

        String message = "";
        String status = "";

        String temp1;
        String pre = "";
        String middle;
        String post = "";
        String strs_h[], strs_f[], strs_m[];
        String s_h_m = "", s_m_f = "", field_s = "";
        Connection con2;
        Statement stmt2;
        ResultSet res2;

        try {
            Connection con5 = dc.database_connection_main();
            Statement stmt5 = con5.createStatement();
            /**getting separators*/
            String q = "SELECT * from job where job_id=" + job_id + ";";
            ResultSet res = stmt5.executeQuery(q);

            while (res.next()) {
                s_h_m = res.getString("seperator_h_m");
                /**separator between header and middle message*/
                s_m_f = res.getString("seperator_m_f");
                /**separator between middle message and footer*/
                field_s = res.getString("field_seperator");
                /**field separator for middle message*/
            }

            con5.close();
        } catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, e);
        }
        String unique_field_cell = "";
        
        try {

            Connection con6 = dc.database_connection_main();
            Statement stmt6 = con6.createStatement();
            /**getting cell numbers for scheduled job*/
            String q = "SELECT * from job_mobile_query where job_id=" + job_id + ";";
            ResultSet res = stmt6.executeQuery(q);
            String qu = "", unique_field = "", cell_field = "", connstr = "";
            while (res.next()) {
                qu = res.getString("query");
                /**query for getting cell numbers*/
                unique_field_cell = res.getString("unique_field");
                /**unique field of query(like roll_no)*/
                cell_field = res.getString("cell_field");
                /**field for cell number*/
                connstr = res.getString("db_connection_name");
                /**connection string for database(which type of database)*/
            }

            con6.close();
            /**getting connection string for cell query database*/
            Connection conn = dc.db_conn(connstr);
            Statement stmtt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet ress = stmtt.executeQuery(qu);
            String un = "", cell;
            ress.last();
            int ii = ress.getRow();
            ress.beforeFirst();

            /**loop for getting cell numbers*/
            while (ress.next()) {
                un = ress.getString(unique_field_cell);
                /**getting unique field of cell query(like roll no)*/
                cell = ress.getString(cell_field);
                /**getting cell number*/
                //getting header message
                try {
                    Connection con7 = dc.database_connection_main();
                    Statement stmt7 = con7.createStatement();
                    /**query for getting header message*/
                    q = "SELECT * from job_query where job_id=" + job_id + " and type='header';";
                    res = stmt7.executeQuery(q);
                    while (res.next()) {
                        qu = res.getString("query");
                        /**retrieving query for header message*/
                        unique_field = res.getString("unique");
                        /**retrieving unique field of header message if available*/
                        connstr = res.getString("conn_string");
                        /**getting connection string for database(which type of database)*/
                        status = res.getString("status");
                        /**header type is static or dynamic*/
                    }

                    con7.close();
                } catch (SQLException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, "SQL Exception: " + ex.toString());
                }
                /**checking for message type(static or dynamic)*/
                if (status.equals("static")) {
                    message = message + qu;
                } else {
                    /**getting number of fields for dynamic header message*/
                    String q1 = qu.substring(0, 6);
                    int i3 = qu.indexOf(q1);
                    int i2 = qu.indexOf("from");
                    String results = "";
                    String r3 = qu.substring(i3 + 7, i2 - 1);
                    Pattern pat = Pattern.compile(",");
                    strs_h = pat.split(r3);
                    if (unique_field != "") {
                        /**if unique field is available than put it in query*/
                        q = qu + " where " + unique_field + "='" + un + "';";
                    } else {
                        q = qu;
                    }

                    try {
                        con2 = dc.db_conn(connstr);
                        stmt2 = con2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                        res2 = stmt2.executeQuery(q);
                        /**executing header message query*/
                        res2.first();
                        Connection con4 = dc.database_connection_main();
                        Statement stmt4 = con4.createStatement();
                        String header_field_query;
                        ResultSet rs;
                        /**retrieving prefix and suffix for header fields */
                        for (int i = 0; i < strs_h.length; i++) {
                            /**query for retrieving prefix and suffix using field name*/
                            header_field_query = "SELECT * from job_pre_post where field='" + strs_h[i] + "' and job_id=" + job_id + " and type='header';";
                            rs = stmt4.executeQuery(header_field_query);
                            while (rs.next()) {
                                pre = rs.getString("prefix");
                                post = rs.getString("postfix");
                            }
                            /**retrieving field value*/
                            middle = res2.getString(strs_h[i]);

                            message = message + " " + pre + " " + middle + " " + post + " ";
                        }

                        con4.close();
                        con2.close();
                    } catch (SQLException e) {
                        javax.swing.JOptionPane.showMessageDialog(null, e);
                    }
                }


                /**middle message generation*/
                con = dc.database_connection_main();
                Statement stmt = con.createStatement();
                /**query for retrieving middle message query,status,connection string and unique field*/
                q = "SELECT * from job_query where job_id=" + job_id + " and type='message';";
                res = stmt.executeQuery(q);
                while (res.next()) {
                    /**retrieving query*/
                    qu = res.getString("query");
                    /**retrieving unique field of query if available*/
                    unique_field = res.getString("unique");
                    /**retrieving connection string of database*/
                    connstr = res.getString("conn_string");
                    status = res.getString("status");
                }
                con.close();
                //checking for message type(static or dynamic)
                if (status.equals("static")) {
                    message = message + qu;
                } else {
                    /**getting no of fields for middle message using query*/
                    String q1 = qu.substring(0, 6);
                    int i3 = qu.indexOf(q1);
                    int i2 = qu.indexOf("from");
                    String results = "";
                    String r3 = qu.substring(i3 + 7, i2 - 1);
                    Pattern pat = Pattern.compile(",");
                    strs_h = pat.split(r3);
                    if (unique_field != "") {
                        /**if unique field is available,put it in query*/
                        q = qu + " where " + unique_field + "='" + un + "';";
                    } else {
                        q = qu;
                    }

                    con2 = dc.db_conn(connstr);
                    stmt2 = con2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                    /**executing middle message query*/
                    res2 = stmt2.executeQuery(q);
                    while (res2.next()) {
                        Connection con4 = dc.database_connection_main();
                        Statement stmt4 = con4.createStatement();
                        String header_field_query;
                        ResultSet rs;
                        /**retrieving prefix and suffix of fields*/
                        for (int i = 0; i < strs_h.length; i++) {

                            header_field_query = "SELECT * from job_pre_post where field='" + strs_h[i] + "' and job_id=" + job_id + " and type='message';";
                            rs = stmt4.executeQuery(header_field_query);
                            while (rs.next()) {
                                pre = rs.getString("prefix");
                                post = rs.getString("postfix");
                            }
                            /**retrieving value of field*/
                            middle = res2.getString(strs_h[i]);

                            message = message + " " + pre + " " + middle + " " + post + " ";
                        }
                        /**concatenating message with field separator*/
                        message = message + field_s;
                        con4.close();
                    }
                    con2.close();
                }
                /**concatenating message with  separator between middle and footer message*/
                message = message + s_m_f;
                /**footer message generation*/
                Connection con8 = dc.database_connection_main();
                Statement stmt8 = con8.createStatement();
                /**query for retrieving query,connection string,status,unique field etc. for footer message*/
                q = "SELECT * from job_query where job_id=" + job_id + " and type='footer';";
                res = stmt8.executeQuery(q);
                while (res.next()) {
                    /**retrieving footer message query*/
                    qu = res.getString("query");
                    /**retrieving unique field for footer message if available*/
                    unique_field = res.getString("unique");
                    /**retrieving connection string for database*/
                    connstr = res.getString("conn_string");
                    /**retrieving type of message(static or dynamic)*/
                    status = res.getString("status");
                }
                con8.close();
                //checking for message type(static or dynamic
                if (status.equals("static")) {
                    message = message + qu;
                } else {
                    /**getting no of fields for footer message*/
                    String q1 = qu.substring(0, 6);
                    int i3 = qu.indexOf(q1);
                    int i2 = qu.indexOf("from");
                    String results = "";
                    String r3 = qu.substring(i3 + 7, i2 - 1);
                    Pattern pat = Pattern.compile(",");
                    strs_h = pat.split(r3);
                    if (unique_field != "") {
                        /**if unique field is available,put it in query*/
                        q = qu + " where " + unique_field + "='" + un + "';";
                    } else {
                        q = qu;
                    }

                    con2 = dc.db_conn(connstr);
                    stmt2 = con2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                    /**executing footer message query*/
                    res2 = stmt2.executeQuery(q);
                    res2.first();
                    Connection con4 = dc.database_connection_main();
                    Statement stmt4 = con4.createStatement();
                    String header_field_query;
                    ResultSet rs;
                    /**retrieving prefix and suffix of footer message fields*/
                    for (int i = 0; i < strs_h.length; i++) {
                        header_field_query = "SELECT * from job_pre_post where field='" + strs_h[i] + "' and job_id=" + job_id + " and type='footer';";
                        rs = stmt4.executeQuery(header_field_query);
                        while (rs.next()) {
                            pre = rs.getString("prefix");
                            post = rs.getString("postfix");
                        }
                        /**retrieving value of field*/
                        middle = res2.getString(strs_h[i]);
                        message = message + " " + pre + " " + middle + " " + post + " ";
                    }
                    con4.close();
                    con2.close();

                }

                /**after generating message put them into queue for sending*/
                Connection con_cell = dc.database_connection_main();
                Statement stmt_cell = con_cell.createStatement();
                /**query for inserting generated message into queue table*/
                String date1=this.now("dd-MM-yyyy");
                String time1=this.now("HH:mm");
                
                String insert_query = "INSERT into queue(cell_no,message,priority,status,job_id,user,roll_no,rdate,rtime) VALUES ('" + cell + "','" + message + "',1,0," + job_id + ",'admin','"+un+"','"+date1+"','"+time1+"');";


                stmt_cell.executeUpdate(insert_query);
                
                message = "";
            }
            conn.close();

        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex.getMessage());

        }


        /**after adding all messages for scheduled job,check the repeat type of job
         *if repeat type is once then set status of schedule to 0
         * if repeat type is monthly,update date of schedule according to month
         * if repeat type is yearly,update date of schedule according to year
         * if repeat type is daily,increment date of schedule
         * if repeat type is end of the month,update accordingly
         */
        try {
            String update_query = null;
            Connection conn = dc.database_connection_main();
            Statement stmtt = conn.createStatement();

            if (type.equals("once")) {
                update_query = "Update schedule set status=0 where schedule_id=" + schedule_id + "";
            } else if (type.equals("monthly")) {
            } else if (type.equals("yearly")) {
            } else if (type.equals("daily")) {
            } else if (type.equals("end of the month")) {
            }
            stmtt.executeUpdate(update_query);
            conn.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, "SQL Exception: " + ex.toString());
        }
    }
}
