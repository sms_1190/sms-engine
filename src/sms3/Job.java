/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

/**
 *
 * @author 
 */
public class Job {

    private String job_name;
    private String Description;
    private String cell_query_conn_string;
    private String cell_query;
    private String cell_field;
    private String unique_field;
    private String header_status;
    private String footer_status;
    private String header_query;
    private String footer_query;
    private String message_query;
    private String header_unique;
    private String footer_unique;
    private String message_unique;
    private String header_conn_string;
    private String footer_conn_string;
    private String message_conn_string;

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getCell_field() {
        return cell_field;
    }

    public void setCell_field(String cell_field) {
        this.cell_field = cell_field;
    }

    public String getCell_query() {
        return cell_query;
    }

    public void setCell_query(String cell_query) {
        this.cell_query = cell_query;
    }

    public String getCell_query_conn_String() {
        return cell_query_conn_string;
    }

    public void setCell_query_conn_String(String cell_query_conn_String) {
        this.cell_query_conn_string = cell_query_conn_String;
    }

    public String getFooter_conn_string() {
        return footer_conn_string;
    }

    public void setFooter_conn_string(String footer_conn_string) {
        this.footer_conn_string = footer_conn_string;
    }

    public String getFooter_query() {
        return footer_query;
    }

    public void setFooter_query(String footer_query) {
        this.footer_query = footer_query;
    }

    public String getFooter_status() {
        return footer_status;
    }

    public void setFooter_status(String footer_status) {
        this.footer_status = footer_status;
    }

    public String getFooter_unique() {
        return footer_unique;
    }

    public void setFooter_unique(String footer_unique) {
        this.footer_unique = footer_unique;
    }

    public String getHeader_conn_string() {
        return header_conn_string;
    }

    public void setHeader_conn_string(String header_conn_string) {
        this.header_conn_string = header_conn_string;
    }

    public String getHeader_query() {
        return header_query;
    }

    public void setHeader_query(String header_query) {
        this.header_query = header_query;
    }

    public String getHeader_status() {
        return header_status;
    }

    public void setHeader_status(String header_status) {
        this.header_status = header_status;
    }

    public String getHeader_unique() {
        return header_unique;
    }

    public void setHeader_unique(String header_unique) {
        this.header_unique = header_unique;
    }

    public String getJob_name() {
        return job_name;
    }

    public void setJob_name(String job_name) {
        this.job_name = job_name;
    }

    public String getMessage_conn_string() {
        return message_conn_string;
    }

    public void setMessage_conn_string(String message_conn_string) {
        this.message_conn_string = message_conn_string;
    }

    public String getMessage_query() {
        return message_query;
    }

    public void setMessage_query(String message_query) {
        this.message_query = message_query;
    }

    public String getMessage_unique() {
        return message_unique;
    }

    public void setMessage_unique(String message_unique) {
        this.message_unique = message_unique;
    }

    public String getUnique_field() {
        return unique_field;
    }

    public void setUnique_field(String unique_field) {
        this.unique_field = unique_field;
    }
}
