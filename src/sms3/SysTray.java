/*
package sms3;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.awt.Image;

import java.awt.MenuItem;

import java.awt.Panel;

import java.awt.PopupMenu;

import java.awt.SystemTray;

import java.awt.TrayIcon;

import java.awt.event.ActionEvent;

import java.awt.event.ActionListener;

import java.awt.image.BufferedImage;

import javax.swing.Icon;

import javax.swing.JOptionPane;

import javax.swing.plaf.metal.MetalIconFactory;

public class SysTray {

    private static Engine en;

    public SysTray(Engine engine) throws AWTException, InterruptedException {
        en = engine;
        TrayIcon icon = new TrayIcon(getImage(),
                "Engine", createPopupMenu());

        icon.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                JOptionPane.showMessageDialog(null,
                        "hiiii");

            }
        });

        SystemTray.getSystemTray().add(icon);

    }

    private static Image getImage() throws HeadlessException {

        Icon defaultIcon = MetalIconFactory.getTreeComputerIcon();
        Image img = new BufferedImage(defaultIcon.getIconWidth(),
                defaultIcon.getIconHeight(),
                BufferedImage.TYPE_4BYTE_ABGR);

        defaultIcon.paintIcon(new Panel(), img.getGraphics(), 0, 0);

        return img;

    }

    private static PopupMenu createPopupMenu() throws
            HeadlessException {

        final PopupMenu menu = new PopupMenu();

        final MenuItem exit = new MenuItem("Exit");

        exit.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                System.exit(0);

            }
        });


        final MenuItem hide_engine = new MenuItem("Hide Engine");
        hide_engine.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {

                en.setVisible(false);
                menu.removeAll();
                final MenuItem engine = new MenuItem("View Engine");
                engine.addActionListener(new ActionListener() {

                    public void actionPerformed(ActionEvent e) {
                        en.change_main();
                        en.setVisible(true);
                        menu.removeAll();

                        menu.add(hide_engine);
                        menu.add(exit);
                    }
                });
                menu.add(engine);
                menu.add(exit);
            }
        });
        final MenuItem engine = new MenuItem("View Engine");
        engine.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                en.change_main();
                en.setVisible(true);
                menu.removeAll();

                menu.add(hide_engine);
                menu.add(exit);
            }
        });

        menu.add(engine);
        menu.add(exit);
        return menu;

    }
}
*/