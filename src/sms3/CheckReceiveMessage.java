/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

/**
 *
 * @author 
 */
public class CheckReceiveMessage {

    private int flag = 0;
    private int storage_location = 0;
    Sender sender;
    private String message;
    private String cell_no;
    private String time;
    DatabaseConnection dc = new DatabaseConnection();
    Connection con;
    Statement stmt;

    public String getCell_no() {
        return cell_no;
    }

    public void setCell_no(String cell_no) {
        this.cell_no = cell_no;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getStorage_location() {
        return storage_location;
    }

    public void setStorage_location(int storage_location) {
        this.storage_location = storage_location;
    }

    public void make_message() {
        sender.parse_message(storage_location);
        con = dc.database_connection_main();
        String query = "INSERT into receive_message (cell_no,message,time)values('" + this.getCell_no() + "','" + this.getMessage() + "','" + this.getTime() + "')";
        try {
            stmt = con.createStatement();
            stmt.executeUpdate(query);
            con.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }

        add_into_queue();
    }

    private void add_into_queue() {
        String cell_no = this.getCell_no();
        String message = this.getMessage();
        Pattern pat = Pattern.compile(" ");
        String strs_m[] = pat.split(message);
        con = dc.database_connection_main();
        String keyword = "";
        String strs[] = null;
        String query, auth_query = null, auth_query_conn_str = null, parameter_missing = null, query_not_satisfied = null;
        String separator_h_m = "", separator_m_f = "", field_separator = "";
        int keyid = 0;
        try {
            stmt = con.createStatement();
            query = "SELECT * from keyword";
            ResultSet rs = stmt.executeQuery(query);

            while (rs.next()) {
                keyword = rs.getString("keyword");
                keyid = rs.getInt("keyword_id");
                auth_query = rs.getString("auth_query");
                auth_query_conn_str = rs.getString("auth_query_conn_str");
                parameter_missing = rs.getString("parameter_missing");
                query_not_satisfied = rs.getString("query_not_satisfied");
                separator_h_m = rs.getString("seperator_h_m");
                separator_m_f = rs.getString("seperator_m_f");
                field_separator = rs.getString("field_seperator");
                if (keyword.equals(strs_m[0])) {
                    break;
                }
            }
            con.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
        if (keyid == 0) {
        } else {
            con = dc.database_connection_main();
            try {
                stmt = con.createStatement();
                query = "SELECT * from keyword_parameters where keyword_id=" + keyid + " order by position;";
                ResultSet rs1 = stmt.executeQuery(query);
                int i = 1;
                strs[0] = keyword;
                while (rs1.next()) {
                    String parameter = rs1.getString("parameter");
                    strs[i] = parameter;
                }
                con.close();
            } catch (SQLException ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }
            if (strs[0].equals(strs_m[0])) {
                if (strs.length == strs_m.length) {
                    for (int j = 1; j < strs.length; j++) {
                        auth_query = auth_query.replace(strs[j], strs_m[j]);
                    }
                    auth_query = auth_query.replace("CELLNO", cell_no);

                    Connection con1 = dc.db_conn(auth_query_conn_str);
                    try {
                        Statement stmt1 = con1.createStatement();
                        ResultSet rs1 = stmt1.executeQuery(auth_query);
                        int p = 0;
                        while (rs1.next()) {
                            p = p + 1;
                        }
                        con.close();
                        if (p == 1) {

                            //create message
                            con = dc.database_connection_main();
                            String q1, r3, status = "", conn_str = "", temp = "", header_query = "";
                            int i3, i2;
                            String strs_h[];
                            //proccessing for header message
                            try {
                                Statement stmt2 = con.createStatement();
                                String q = "SELECT * from keyword_query where keyword_id=" + keyid + " and type='header';";
                                ResultSet rs2 = stmt2.executeQuery(q);
                                while (rs2.next()) {
                                    header_query = rs2.getString("query");
                                    status = rs2.getString("status");
                                    conn_str = rs2.getString("conn_string");
                                }
                                con.close();
                                if (status.equals("static")) {
                                    temp = temp + header_query;
                                } else {
                                    con = dc.db_conn(conn_str);
                                    Statement stmt3 = con.createStatement();
                                    header_query = header_query.replace("<", "'");
                                    //System.out.println(query);
                                    for (int h = 0; h < strs.length; h++) {
                                        header_query = header_query.replace(strs[h], strs_m[h + 1]);
                                    }
                                    q1 = header_query.substring(0, 6);

                                    i3 = header_query.indexOf(q1);
                                    i2 = header_query.indexOf("from");
                                    String results = null;
                                    r3 = header_query.substring(i3 + 7, i2 - 1);
                                    pat = Pattern.compile(",");
                                    strs_h = pat.split(r3);
                                    //System.out.println(query);
                                    //System.out.println(strs_h.length);
                                    String field_value, prefix = "", suffix = "";
                                    ResultSet rs3 = stmt3.executeQuery(header_query);
                                    while (rs3.next()) {
                                        for (int i = 0; i < strs_h.length; i++) {

                                            field_value = rs3.getString(strs_h[i]);
                                            con1 = dc.database_connection_main();
                                            Statement stmt4 = con1.createStatement();
                                            String query_header = "SELECT * from keyword_pre_post where keyword_id=" + keyid + " and type='header' and field='" + strs_h[i] + "';";
                                            ResultSet rs4 = stmt4.executeQuery(query_header);
                                            while (rs3.next()) {
                                                prefix = rs3.getString("prefix");
                                                suffix = rs3.getString("postfix");
                                            }
                                            con1.close();
                                            temp = temp + " " + prefix + " " + field_value + " " + suffix + " ";

                                        }
                                    }
                                    con.close();
                                }
                                //System.out.println(temp);
                            } catch (SQLException ex) {
                                javax.swing.JOptionPane.showMessageDialog(null, ex);
                            }
                            temp = temp + separator_h_m;
                            //middle message
                            String message_query = "";
                            con = dc.database_connection_main();
                            try {
                                Statement stmt2 = con.createStatement();
                                String q = "SELECT * from keyword_query where keyword_id=" + keyid + " and type='message';";
                                ResultSet rs2 = stmt2.executeQuery(q);
                                while (rs2.next()) {
                                    message_query = rs2.getString("query");
                                    status = rs2.getString("status");
                                    conn_str = rs2.getString("conn_string");
                                }
                                con.close();
                                if (status.equals("static")) {
                                    temp = temp + message_query;
                                } else {
                                    con = dc.db_conn(conn_str);
                                    Statement stmt3 = con.createStatement();
                                    message_query = message_query.replace("<", "'");
                                    //System.out.println(query);
                                    for (int h = 0; h < strs.length; h++) {
                                        message_query = message_query.replace(strs[h], strs_m[h + 1]);
                                    }
                                    q1 = message_query.substring(0, 6);

                                    i3 = message_query.indexOf(q1);
                                    i2 = message_query.indexOf("from");
                                    String results = null;
                                    r3 = message_query.substring(i3 + 7, i2 - 1);
                                    pat = Pattern.compile(",");
                                    strs_h = pat.split(r3);
                                    //System.out.println(query);
                                    //System.out.println(strs_h.length);
                                    String field_value, prefix = "", suffix = "";
                                    ResultSet rs3 = stmt3.executeQuery(message_query);
                                    while (rs3.next()) {
                                        for (int i = 0; i < strs_h.length; i++) {

                                            field_value = rs3.getString(strs_h[i]);
                                            con1 = dc.database_connection_main();
                                            Statement stmt4 = con1.createStatement();
                                            String query_header = "SELECT * from keyword_pre_post where keyword_id=" + keyid + " and type='message' and field='" + strs_h[i] + "';";
                                            ResultSet rs4 = stmt4.executeQuery(query_header);
                                            while (rs3.next()) {
                                                prefix = rs3.getString("prefix");
                                                suffix = rs3.getString("postfix");
                                            }
                                            con1.close();
                                            temp = temp + " " + prefix + " " + field_value + " " + suffix + " ";
                                        }
                                        //put field separator in middle message
                                        temp = temp + field_separator;
                                    }
                                    con.close();
                                }
                                //System.out.println(temp);
                            } catch (SQLException ex) {
                                javax.swing.JOptionPane.showMessageDialog(null, ex);
                            }
                            temp = temp + separator_m_f;
                            //footer message
                            String footer_query = "";
                            con = dc.database_connection_main();
                            try {
                                Statement stmt2 = con.createStatement();
                                String q = "SELECT * from keyword_query where keyword_id=" + keyid + " and type='footer';";
                                ResultSet rs2 = stmt2.executeQuery(q);
                                while (rs2.next()) {
                                    footer_query = rs2.getString("query");
                                    status = rs2.getString("status");
                                    conn_str = rs2.getString("conn_string");
                                }
                                con.close();
                                if (status.equals("static")) {
                                    temp = temp + footer_query;
                                } else {
                                    con = dc.db_conn(conn_str);
                                    Statement stmt3 = con.createStatement();
                                    footer_query = footer_query.replace("<", "'");
                                    //System.out.println(query);
                                    for (int h = 0; h < strs.length; h++) {
                                        footer_query = footer_query.replace(strs[h], strs_m[h + 1]);
                                    }
                                    q1 = footer_query.substring(0, 6);

                                    i3 = footer_query.indexOf(q1);
                                    i2 = footer_query.indexOf("from");
                                    String results = null;
                                    r3 = footer_query.substring(i3 + 7, i2 - 1);
                                    pat = Pattern.compile(",");
                                    strs_h = pat.split(r3);
                                    //System.out.println(query);
                                    //System.out.println(strs_h.length);
                                    String field_value, prefix = "", suffix = "";
                                    ResultSet rs3 = stmt3.executeQuery(footer_query);
                                    while (rs3.next()) {
                                        for (int i = 0; i < strs_h.length; i++) {

                                            field_value = rs3.getString(strs_h[i]);
                                            con1 = dc.database_connection_main();
                                            Statement stmt4 = con1.createStatement();
                                            String query_header = "SELECT * from keyword_pre_post where keyword_id=" + keyid + " and type='footer' and field='" + strs_h[i] + "';";
                                            ResultSet rs4 = stmt4.executeQuery(query_header);
                                            while (rs3.next()) {
                                                prefix = rs3.getString("prefix");
                                                suffix = rs3.getString("postfix");
                                            }
                                            con1.close();
                                            temp = temp + " " + prefix + " " + field_value + " " + suffix + " ";
                                        }
                                    }
                                    con.close();
                                }
                                //System.out.println(temp);
                            } catch (SQLException ex) {
                                javax.swing.JOptionPane.showMessageDialog(null, ex);
                            }
                            con = dc.database_connection_main();
                            Statement stmt = con.createStatement();
                            query = "INSERT into queue (cell_no,message,priority,status)VALUES('" + cell_no + "','" + temp + "',4,0);";
                            stmt.executeUpdate(query);
                            con.close();
                        } else {
                            //query not satisfied message
                            con = dc.database_connection_main();
                            Statement stmt = con.createStatement();
                            if (query_not_satisfied.equals("") || query_not_satisfied == null) {
                            } else {
                                query = "INSERT into queue (cell_no,message,priority,status)VALUES('" + cell_no + "','" + query_not_satisfied + "',4,0);";
                                stmt.executeUpdate(query);
                            }
                            con.close();

                        }
                    } catch (SQLException ex) {
                    }


                } else {
                    //parameter missing message
                    con = dc.database_connection_main();
                    Statement stmt;
                    try {
                        stmt = con.createStatement();
                        if (parameter_missing.equals("") || parameter_missing == null) {
                        } else {
                            query = "INSERT into queue (cell_no,message,priority,status)VALUES('" + cell_no + "','" + parameter_missing + "',4,0);";
                            stmt.executeUpdate(query);
                        }
                        con.close();
                    } catch (SQLException ex) {
                        Logger.getLogger(CheckReceiveMessage.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
            } else {
                //keyword not mached message
            }
        }
    }
}
