package sms3;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;

public class InitialFrame extends JFrame
{
	public InitialFrame()
	{
	setSize(800, 625);
	setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	Container pane = getContentPane();
	FlowLayout flo = new FlowLayout();
	pane.setLayout(flo);
	InitialPanel Date = new InitialPanel();
	pane.add(Date);
        setContentPane(Date);
	InitialPanel Time = new InitialPanel();
	pane.add(Time);
	setContentPane(Time);
	setVisible(true);
        }

public static void main (String[] arguments)
{
	InitialFrame a1 = new InitialFrame();
}

public class InitialPanel extends JPanel
{
	public InitialPanel()
	{
         String Current_Date = getDate();
	 JLabel Date = new JLabel(Current_Date);
	 Date.setForeground(Color.blue);
	 add(Date);
         String Current_Time = getTime();
	 JLabel Time = new JLabel(Current_Time);
	 Time.setForeground(Color.blue);
	 add(Time);

	}

	String getDate()
{
String Date;
 // get current Date
    Calendar now = Calendar.getInstance();
    int month = now.get(Calendar.MONTH) + 1;
    int day = now.get(Calendar.DAY_OF_MONTH);
    int year = now.get(Calendar.YEAR);

    Date = month + "/" + day + "/" + year;
    return Date;
}

	String getTime()
{
String Time;
 // get current Time
    Calendar now = Calendar.getInstance();
    int hour = now.get(Calendar.HOUR_OF_DAY);
    int minute = now.get(Calendar.MINUTE);
    int second = now.get(Calendar.SECOND);

    Time = hour + ":" + minute + ":" + second;
    return Time;
}

}
}