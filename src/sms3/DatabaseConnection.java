/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.sql.*;

/**
 *
 * @author 
 */
/**
 *
 * Class for creating database connection of any type of database
 */
public class DatabaseConnection {

    Connection con;

    /**function for creating connection with main database of application*/
    public Connection database_connection_main() {
        try {
            Class.forName("sun.jdbc.odbc.JdbcOdbcDriver");
            String url = "jdbc:odbc:Driver={Microsoft Access Driver "
                    + "(*.mdb, *.accdb)};DBQ=.\\database\\sms.mdb";
            con = DriverManager.getConnection(url);
        } catch (SQLException e) {
            javax.swing.JOptionPane.showMessageDialog(null, e);
        } catch (ClassNotFoundException cE) {
            javax.swing.JOptionPane.showMessageDialog(null, cE);
        }
        /**returning connection*/
        return con;
    }

    /**function for creating connection with user databases*/
    public Connection db_conn(String connstring) {
        /**creating connection with main database for retrieving configuration of database*/
        con = database_connection_main();
        if (con.equals(null)) {
        } else {
            String cfn = null;
            String cs = null;
            String db = null;
            String user_name = null;
            String password = null;
            String server = null;
            String port = null;
            String db_type = null;
            try {
                Statement stmt = con.createStatement();
                /**retrieving configuration of database*/
                String query = "SELECT * FROM database_config where status=1 and db_connection_name='" + connstring + "';"; // define query
                ResultSet answers = stmt.executeQuery(query);

                while (answers.next()) {
                    /**retrieving class for name of database*/
                    cfn = answers.getString("class_for_name");
                    /**retrieving connection string of database*/
                    cs = answers.getString("connection_string");
                    /**retrieving name of database*/
                    db = answers.getString("db_name");
                    /**retrieving user name of database*/
                    user_name = answers.getString("user_name");
                    /**retrieving password of database*/
                    password = answers.getString("password");
                    /**retrieving server name of database*/
                    server = answers.getString("server_name");
                    /**retrieving port no of database*/
                    port = answers.getString("port_no");
                    /**retrieving type of database*/
                    db_type = answers.getString("db_type");
                }
            } catch (SQLException e) {
                javax.swing.JOptionPane.showMessageDialog(null, e);
            }

            String url = null;
            try {
                Class.forName(cfn);
                if (db_type.equals("Access")) {
                    url = cs + db;

                } else if (db_type.equals("Oracle")) {
                } else if (db_type.equals("Mysql")) {
                } else if (db_type.equals("Excel")) {
                }
                /**checking for connection*/
                con = DriverManager.getConnection(url, user_name, password);

            } catch (SQLException e) {
                javax.swing.JOptionPane.showMessageDialog(null, "DFgdf" + e);
                con = null;
            } catch (ClassNotFoundException cE) {
                javax.swing.JOptionPane.showMessageDialog(null, "Sdfsdf" + cE);
                con = null;
            }

        }
        /**returning connection to database*/
        return con;
    }
}
