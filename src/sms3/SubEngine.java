/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.util.TimerTask;

/**
 *
 * @author 
 */
/**
 * This is the sub part of engine for calling instance of job scheduling,checking network,
 * refresh table of engine,sending messages classes etc.
 *
 */
public class SubEngine extends TimerTask {

    //variables for engine and sender class
    Engine engine;
    Sender sender;

    SubEngine() {
        //Empty constructor
    }

    SubEngine(Engine en, Sender send) {
        /**
         * Constructor of sub engine
         */
        engine = en;
        sender = send;
        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        java.util.Timer t1 = new java.util.Timer();
        //schedule the checking of job scheduling task
        CheckingJobScheduling cjs = new CheckingJobScheduling(5000, engine);
        t1.schedule(cjs, 0, 5000);
        java.util.Timer t2 = new java.util.Timer();
        //schedule the checking network strength task
        CheckingNetwork cn = new CheckingNetwork(500, engine, sender);
        t2.schedule(cn, 0, 500);
        java.util.Timer t3 = new java.util.Timer();
        //schedule the refreshing table of engine task
        RefreshTableOfEngine rtoe = new RefreshTableOfEngine(7000, engine, sender);
        t3.schedule(rtoe, 0, 7000);
        //schedule the sending message task
        java.util.Timer t4 = new java.util.Timer();
        SendingMessage sm = new SendingMessage(engine, sender);
        t4.schedule(sm, 3000);

    }
}
