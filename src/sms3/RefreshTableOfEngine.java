/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.util.TimerTask;

/**
 *
 * @author 
 */
/**
 Class for refreshing tables of engine after some time interval*/
public class RefreshTableOfEngine extends TimerTask {

    private int timerInterval;
    int signal_strength;
    /**instance of engine and sender class*/
    Engine engine;
    Sender sender;

    /**constructor for class which initialize engine and sender instances*/
    public RefreshTableOfEngine(int timeInterval, Engine en,Sender send) {
        this.timerInterval = timeInterval;
        engine = en;
        sender=send;
    }

    @Override
    public void run() {
        /**calling refreshtable function of engine*/
        engine.refreshtable();
    }
}
