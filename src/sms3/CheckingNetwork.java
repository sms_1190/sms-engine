/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.util.TimerTask;

/**
 *
 * @author 
 */
public class CheckingNetwork extends TimerTask {

    /**
     * This class is for checking network strength and display the strength of network on engine
     */
    private int timerInterval;
    /**variable for signal strength*/
    int signal_strength;
    /**instance of engine.java class*/
    Engine engine;
    /**instance of sender.java class*/
    Sender sender;

    /**
     * Constructor which initialize the engine,sender instance and time interval for scheduling
     */
    public CheckingNetwork(int timeInterval, Engine en, Sender send) {

        this.timerInterval = timeInterval;
        engine = en;
        sender = send;
    }

    public void run() {
        /**checking and getting signal strength using check_signal function of sender class
         *and setting network strength on engine.
         * Sending AT+CSQ command for getting signal strength
         */
        signal_strength = sender.check_signal("AT+CSQ\r\n");
        if (signal_strength == 0) {
        } else {
            engine.network_setting(signal_strength);
        }
    }
}
