/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;

/**
 *
 * @author 
 */
public class MakeCSVForreceivedMessage {

    DatabaseConnection dc = new DatabaseConnection();
    Connection con;
    Statement stmt;
    String query;

    public MakeCSVForreceivedMessage(String qu) {
        query = qu;
        generateCsvFile(".\\database\\test.csv");
    }

    private void generateCsvFile(String sFileName) {
        try {
            FileWriter writer = new FileWriter(sFileName);
            try {
                con = dc.database_connection_main();
                stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet rs = stmt.executeQuery(query);

                String cell = "";
                String c, c1;
                int i = 0;
                rs.last();
                i = rs.getRow();
                rs.beforeFirst();
                if (i <= 0) {
                    javax.swing.JOptionPane.showMessageDialog(null, "No row selected.");
                } else {
                    i = 1;

                    String status = "Pending";

                    writer.append("Cell No");
                    writer.append(',');
                   
                    writer.append("Message");
                    writer.append(',');
                    
                    writer.append("Time");
                    writer.append(',');
                    
                    writer.append('\n');
                    while (rs.next()) {
                        
                        writer.append(rs.getString("cell_no"));
                        writer.append(',');
                       
                        writer.append(rs.getString("message"));
                        writer.append(',');
                        
                        writer.append(rs.getString("time"));
                        writer.append(',');
                        
                        writer.append('\n');
                        i = i + 1;
                    }
                    con.close();
                    writer.flush();
                    writer.close();
                    try {
                        //Path for folder or file that you want to open
                        //In my case i will open C:\ drive
                        //So create a String like below
                        //Don't forget to change \ to \\


                        //Execute command
                        //Command that you must know : rundll32 SHELL32.DLL,ShellExec_RunDLL File_Or_Folder_To_Open
                        Runtime.getRuntime().exec("rundll32 SHELL32.DLL,ShellExec_RunDLL \"" + sFileName + "\"");
                    } catch (Exception ex) {
                        javax.swing.JOptionPane.showMessageDialog(null, ex);
                    }
                }
            } catch (SQLException ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }
            //generate whatever data you want


        } catch (IOException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
    }
}
