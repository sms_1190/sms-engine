/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * Engine.java
 *
 * Created on 28 Jun, 2010, 11:48:50 AM
 */
/*
 * Engine is the main window of application.
 * From here user can see pending messages,failed messsages,sent messages;
 * In engine there are other threads for receiving messages from port,checking network,sending messages and for scheduling purpose.
 * Engine will create subengine for scheduling threads.
 */
package sms3;

//Import libraries
import java.awt.SystemTray;             
import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.*;    //
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.comm.*;//for COM port
import java.sql.*;//for database


/**
 *
 * @author 
 */
public class Engine extends javax.swing.JFrame {

    //variable for serialport
    SerialPort serial;
    //creating database connection instance
    DatabaseConnection dc = new DatabaseConnection();
    //variable for database connection
    Connection con;
    //creating receiving messages instance
    CheckReceiveMessage crm=new CheckReceiveMessage();
    //creating sender instatnce
    Sender sender=new Sender(crm);
    /** Creates new form Engine */

    public Engine(){
            
            addWindowListener(new WindowAdapter()
            {
                public void windowClosing(WindowEvent e)                
                {                    
                    setVisible(false);
                    javax.swing.JOptionPane.showMessageDialog(rootPane,
                            "The system is still running in background\n " );
                }
            });

        initComponents();        
        call_subengine();
        installTray();    
    }

    public void installTray()
    {       
        final TrayIcon trayIcon;        
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        if (SystemTray.isSupported())
        {
            SystemTray tray = SystemTray.getSystemTray();
            Image image = Toolkit.getDefaultToolkit().getImage("./images/sms-icon.jpg");

            MouseListener mouseListener = new MouseListener()
            {
                public void mouseClicked(MouseEvent e)
                {
                    //System.out.println("Tray Icon - Mouse clicked!");
                    
                }

                public void mouseEntered(MouseEvent e)
                {
                    //System.out.println("Tray Icon - Mouse entered!");
                }

                public void mouseExited(MouseEvent e)
                {
                    //System.out.println("Tray Icon - Mouse exited!");
                }

                public void mousePressed(MouseEvent e)
                {
                    //System.out.println("Tray Icon - Mouse pressed!");
                }

                public void mouseReleased(MouseEvent e)
                {
                    //System.out.println("Tray Icon - Mouse released!");
                }
        };

        ActionListener exitListener = new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                //System.out.println("Exiting...");
                int reply=javax.swing.JOptionPane.showConfirmDialog(rootPane, "Are you sure you want exit?","SMS Manager",1);
                if(reply==0)
                    System.exit(0);
            }
        };

        PopupMenu popup = new PopupMenu();
        MenuItem defaultItem = new MenuItem("Exit");
        defaultItem.addActionListener(exitListener);
        popup.add(defaultItem);

        trayIcon = new TrayIcon(image, "SMS Manager", popup);

        ActionListener actionListener = new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {              
                   // double click
                setVisible(true);
            }
        };

        trayIcon.setImageAutoSize(true);
        trayIcon.addActionListener(actionListener);
        trayIcon.addMouseListener(mouseListener);

        try
        {
            tray.add(trayIcon);
        }
        catch (AWTException e)
        {
            System.err.println("TrayIcon could not be added.");
        }
      }
      else
      {  //  System Tray is not supported
      }
    }

    public void change_main()
    {
        call_main.setEnabled(false);
    }

    public void enable_main()
    {
        call_main.setEnabled(true);
    }
    /**
     * Purpose : for creating instance of subengine
     */
    public void call_subengine() {
        SubEngine se = new SubEngine(this,sender);
    }

    /**
     * Purpose : for updating network strength on window using getting strength from network checking
     * This function get value of network strength from 1 to 31 and using this network strength will be displayed on window.
     */
    public void network_setting(int strength) {
        if (strength == 0) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/0.jpg")));
        } else if(strength >= 1 && strength <= 2) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/1.jpg")));
        } else if (strength > 2 && strength <= 4) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/2.jpg")));
        } else if (strength > 4 && strength <= 6) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/3.jpg")));
        } else if (strength > 6 && strength <= 9) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/4.jpg")));
        } else if (strength > 9 && strength <= 12) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/5.jpg")));
        } else if (strength > 12 && strength <= 15) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/6.jpg")));
        } else if (strength > 15 && strength <= 18) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/7.jpg")));
        }else if (strength > 18 && strength <= 20) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/8.jpg")));
        }else if (strength > 20 && strength <= 22) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/9.jpg")));
        }else if (strength > 22 && strength <= 24) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/10.jpg")));
        }else if (strength > 24 && strength <= 26) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/11.jpg")));
        }else if (strength > 26 && strength <= 28) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/12.jpg")));
        }else if (strength > 28) {
            signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/13.jpg")));
        }

    }

    /**
     * Purpose : Function for updating status of sending message on window.
     */
    public void status_message(String cell, int flag) {
        if (flag == 1) {
            status.setText("Sent Message to : " + cell);
        } else if (flag == 0) {
            status.setText("Sending Message to : " + cell);
        }
    }

    /** This method is called from within the constructor to
     * 
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        signal = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        call_main = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pscroll = new javax.swing.JScrollPane();
        pending_table = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        sent_table = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        failed_table = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        status = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SMS Engine");
        setBackground(new java.awt.Color(255, 255, 255));
        setBounds(new java.awt.Rectangle(500, 130, 500, 400));

        signal.setBackground(new java.awt.Color(255, 255, 255));
        signal.setForeground(new java.awt.Color(255, 255, 255));
        signal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        signal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/zero.jpg"))); // NOI18N

        jPanel1.setPreferredSize(new java.awt.Dimension(418, 200));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 393, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 9, Short.MAX_VALUE)
        );

        call_main.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/return.jpg"))); // NOI18N
        call_main.setToolTipText("Login");
        call_main.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                call_mainActionPerformed(evt);
            }
        });

        pending_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        pending_table.getTableHeader().setResizingAllowed(false);
        pending_table.getTableHeader().setReorderingAllowed(false);
        pscroll.setViewportView(pending_table);

        jTabbedPane1.addTab("Pending Messsages", pscroll);

        sent_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        sent_table.getTableHeader().setResizingAllowed(false);
        sent_table.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(sent_table);

        jTabbedPane1.addTab("Sent Messages", jScrollPane2);

        jScrollPane3.setPreferredSize(new java.awt.Dimension(452, 412));

        failed_table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        failed_table.getTableHeader().setResizingAllowed(false);
        failed_table.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(failed_table);

        jTabbedPane1.addTab("Failed Messages", jScrollPane3);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/sms3/towerBlueIcon.png"))); // NOI18N
        jLabel1.setToolTipText("Signal Quality");
        jLabel1.setBorder(javax.swing.BorderFactory.createCompoundBorder());

        status.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(call_main, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 170, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(signal, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE)
                    .addComponent(status, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 393, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(signal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(call_main, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(status, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Purpose : Function for creating instance of main window with authentication
     * So first generate login window for authentication.
     */
    private void call_mainActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_call_mainActionPerformed

        //Creating instance of login form with passing parameter of sender and engine
        Login lgn = new Login(this,sender);
        lgn.setVisible(true);
        call_main.setEnabled(false);
        this.setVisible(false);

        /*
        try {
            SysTray ft = new SysTray(this);
        } catch (AWTException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(Engine.class.getName()).log(Level.SEVERE, null, ex);
        }
*/
    }//GEN-LAST:event_call_mainActionPerformed

    /**
     * Purpose : Function for refreshing table of pending,sent and failed messages
     */
    void refreshtable() {
        int status;
        String cell_no;
        String message;
        //int job_id;
        String user;


        //setting default table model for pending message table
        javax.swing.table.DefaultTableModel t1 = new javax.swing.table.DefaultTableModel();
        pending_table.setModel(t1);

        //adding colums to table(cell no,message,job id)
        t1.addColumn((Object) "Cell No.");
        t1.addColumn((Object) "Message");
        //t1.addColumn((Object) "Job Id");
        t1.addColumn((Object) "User");

        //setting default table model for sent message table
        javax.swing.table.DefaultTableModel t2 = new javax.swing.table.DefaultTableModel();
        sent_table.setModel(t2);

        //adding colums to table(cell no,message,job id)
        t2.addColumn((Object) "Cell No.");
        t2.addColumn((Object) "Message");
        //t2.addColumn((Object) "Job Id");
        t2.addColumn((Object) "User");

        //setting default table model for failed message table
        javax.swing.table.DefaultTableModel t3 = new javax.swing.table.DefaultTableModel();
        failed_table.setModel(t3);

        //adding colums to table(cell no,message,job id)
        t3.addColumn((Object) "Cell No.");
        t3.addColumn((Object) "Message");
        //t3.addColumn((Object) "Job Id");
        t3.addColumn((Object) "User");

        try {
            //getting connection of database
            con = dc.database_connection_main();
            Statement stmt = con.createStatement();
            //query for getting messages from queue using priority
            String query = "SELECT * from queue order by priority desc;";
            //executing query
            ResultSet rs = stmt.executeQuery(query);
            //getting result of query
            while (rs.next()) {
                status = rs.getInt("status");
                cell_no = rs.getString("cell_no");
                message = rs.getString("message");
                //job_id = rs.getInt("job_id");
                user = rs.getString("user");
                /*
                 * if status of message is 0,then this message status is pending
                 * so put this message in pending table
                 * if status of message is 1,then this message status is sent
                 * so put this message in sent table
                 * if status of message is 2,then this message status is failed
                 * so put this message in failed table
                 */
                if (status == 0) {
                    t1.addRow(new Object[]{cell_no, message, user});
                } else if (status == 1) {
                    t2.addRow(new Object[]{cell_no, message, user});
                } else if (status == 2) {
                    t3.addRow(new Object[]{cell_no, message, user});
                }
            }

            con.close();
        } catch (SQLException ex) {
            //exception error
            javax.swing.JOptionPane.showMessageDialog(rootPane, ex.getMessage());
            javax.swing.JOptionPane.showMessageDialog(rootPane, ex);
        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {

                new Engine().setVisible(true);

            }            
        });        
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton call_main;
    private javax.swing.JTable failed_table;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable pending_table;
    private javax.swing.JScrollPane pscroll;
    private javax.swing.JTable sent_table;
    private javax.swing.JLabel signal;
    private javax.swing.JTextField status;
    // End of variables declaration//GEN-END:variables
    
}
