/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/*
 * Class for sending message to port,checking netowrk strength and notify engine about strength,
 * receiving message from port etc.
 */
package sms3;

//import libraries
import java.util.TooManyListenersException;//for listener
import javax.comm.*;//for COM port
import java.io.*;//for input output operation

public class Sender {

    //variable for serialport
    SerialPort serialPort;
    //command for setting port for writing purpose
    private String commands = "AT+CMGF=1\r\n";
    //variable for input stream
    private InputStream inputStream;
    //variable for output stream
    private OutputStream outputStream;
    //flag variables
    int result = 0;
    int lock = 0;
    String l;
    //variable of datainput stream
    DataInputStream is;
    //variable for byte buffer
    byte[] readBuffer = new byte[20];
    //variable for checkreceivemessage instance
    CheckReceiveMessage crm;

    /*
     * Purpose : function for cgetting signal strength
     */
    public int getSignal_strength() {
        return signal_strength;
    }

    /*
     * Purpose : function for setting signal strength
     */
    public void setSignal_strength(int signal_strength) {
        this.signal_strength = signal_strength;
    }
    int flag = 0;
    //variable for CTRL + Z
    char ctrlz = (char) 26;
    //variable for printstream
    private PrintStream os = null;
    //inialize signal_strength variable to 0
    private int signal_strength = 0;

    public Sender(CheckReceiveMessage rm) {
        crm = rm;

        //creating instance of portintialize class
        PortInitilize p = new PortInitilize();
        try {
            //getting serialport

            serialPort = p.port_init_run();

        } catch (NoSuchPortException ex) {
            //no such port exception            
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        } catch (TooManyListenersException ex) {
            //too many listener exception
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
        /*
        try {
            //getting input stream for serial port


            
            inputStream = serialPort.getInputStream();

            /*try {
            serialPort.addEventListener(this);
            } catch (TooManyListenersException e) {
            //system.out.println("Exception in Adding Listener" + e);
            }
            //setting notification if data available on serial port
            serialPort.notifyOnDataAvailable(true);
        } catch (IOException ex) {

            //i/p o/p exception
            
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
        */
        try {
            //creating new datainputstream
            is = new DataInputStream(serialPort.getInputStream());
            //creating new printstream
            os = new PrintStream(serialPort.getOutputStream(), true);
            //getting outputstream for serialport
            outputStream = serialPort.getOutputStream();
            //sending("AT+CNMI=1,1,0,0,0\r\n", 0);
            //expect("OK");
        } catch (IOException ex) {
            //i/p o/p exception
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
    }

    /* public void serialEvent(SerialPortEvent event) {
    int temp = 0;
    switch (event.getEventType()) {
    case SerialPortEvent.BI:
    case SerialPortEvent.OE:
    case SerialPortEvent.FE:
    case SerialPortEvent.PE:
    case SerialPortEvent.CD:
    case SerialPortEvent.CTS:
    case SerialPortEvent.DSR:
    case SerialPortEvent.RI:
    case SerialPortEvent.OUTPUT_BUFFER_EMPTY:
    break;
    case SerialPortEvent.DATA_AVAILABLE: {
    try {
    l = is.readLine();
    //system.out.println(l);
    } catch (IOException ex) {
    Logger.getLogger(Sender.class.getName()).log(Level.SEVERE, null, ex);
    }



    try {
    while (inputStream.available() > 0) {
    int numBytes = inputStream.read(readBuffer);
    //system.out.print(numBytes);
    //system.out.println(new String(readBuffer));
    }


    } catch (IOException e) {
    //system.out.println(e);
    }




    byte[] readBuffer = new byte[100];
    int r = 0, p = 0;
    String tt = "";
    try {
    while ((r = inputStream.read(readBuffer)) != -1) {
    tt = new String(readBuffer, 0, r);

    p = p + 1;
    //system.out.println(p);
    if (p % 2 == 0) {
    if (tt.indexOf("OK") > 0) {
    //system.out.println("success");
    setbit(1);

    } else if (tt.indexOf("ERROR") > 0) {
    //system.out.println("error");
    setbit(0);

    }
    }
    //system.out.print(new String(readBuffer, 0, r));
    }
    } catch (Exception e) {
    e.printStackTrace(//system.out);
    }
    break;
    }



    } //switch
    }*/
    public void setbit(int flag) {

        result = flag;

    }

    /*
     * Purpose : function for sending messages or any commands to serialport
     */
    public void sending(String str, int t) throws IOException {
        ////system.out.println("Sending:" + str);
        if (t == 1) {
            //if this is last line of message then write CTRL+Z after writing message
            outputStream.write(str.getBytes());
            outputStream.write(26);
            outputStream.write(13);
        } else {
            //else write only message string or command
            outputStream.write(str.getBytes());
        }
        //setting flag variable to 1
        flag = 1;
    }

    /*
     * Purpose : function for expecting some reply from serialport after sending messages or command to serialport
     */
    public int expect(String str) throws IOException {
        int r = 0;
        //system.out.println("Expected :" + str);
        String ll;
        //reading line from serialport
        ll = is.readLine();
        //System.out.println("ABC:"+ll);
        if (ll != null) {
            //find the index of expecting string
            //if found then set result variable to 1
            if (ll.indexOf(str) >= 0) {
                r = 1;
            } else if (ll.indexOf("ERROR") >= 0) {
                //if index of error is found then set result variable to 2
                r = 2;
            } else if (ll.indexOf("+CMTI:") >= 0) {
                //if index of +CMTI: is found then get the storage location of received message and store it
                if (ll.contains(",")) {
                    //parsing the string and getting the storage location
                    int index = ll.indexOf(",");
                    int storage = Integer.parseInt(ll.substring(index, 5));
                    //setting storage location and flag(received message is available)
                    crm.setStorage_location(storage);
                    crm.setFlag(1);
                }
            } else {
                r = 0;
            }
        }
        return r;
    }

    /*
     * Purpose : function for expecting signal strength string
     */
    public int expect_signal(String str) throws IOException {
        int r = 0;
        int sig_index;
        String sig_strength;
        //system.out.println("Expected :" + str);
        String ll;
        //reading line from serial port
        ll = is.readLine();

        if (ll != null) {
            //find the index of expecting string
            //if found then set result variable to 1
            if (ll.indexOf(str) >= 0) {
                r = 1;
                //parsing the string and getting signal strength
                sig_index = ll.indexOf(str) + 5;

                sig_strength = ll.substring(sig_index, sig_index + 2);

                if (sig_strength.contains(",")) {
                    sig_strength = ll.substring(sig_index, sig_index + 1);
                }
                //setting signal strength
                setSignal_strength(Integer.parseInt(sig_strength));
                
            } else if (ll.indexOf("ERROR") >= 0) {
                //if index of error is found then set result variable to 2
                r = 2;
            } else {
                r = 0;
            }
        }
        return r;
    }

    /*
     * Purpose : function for sending message to cell no using AT Commands
     */
    public int send(String mbno, String msg) {
        result = 0;
        if (lock == 0) {
            lock = 1;

            try {
                //getting output stream
                outputStream = serialPort.getOutputStream();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
                //sending AT+CMGF=1 command
                sending(commands, 0);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
                //expecting OK reply
                while ((result = expect("OK")) != 1) {
                    if (result == 2) {
                        break;
                    }
                }

                String mb = "AT+CMGS=\"+91" + mbno + "\"\r\n";
                //sending cell no to serialport using AT+CMGS command
                sending(mb, 0);
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);

                }
                //expecting '>' reply for writing message to serialport
                while ((result = expect(">")) != 1) {
                    if (result == 2) {
                        break;
                    }
                }

                //sending message to serialport
                sending(msg, 1);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);

                }
                //expecting 'OK' reply
                while ((result = expect("OK")) != 1) {
                    if (result == 2) {
                        break;
                    }
                }

            } catch (Exception ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }
            lock = 0;
        }

        return result;
    }

    /*
     * Purpose : function for checking and getting signal strength
     */
    public int check_signal(String command) {
        int rr = 0;
        if (lock == 0) {
            lock = 2;
            try {
                //getting output stream
                outputStream = serialPort.getOutputStream();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
                //sending AT+CSQ command(for getting signal strength
                sending(command, 0);

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
                //getting signal
                while ((rr = expect_signal("CSQ:")) != 1);

            } catch (Exception ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }
            lock = 0;
        }
        return getSignal_strength();
    }

    /*
     * Purpose : function for parsing and receiving message from storage location
     */
    public void parse_message(int storage_location) {
        String str = "AT+CMGR=" + storage_location + "\r\n";
        try {
            //sending AT+CMGR command with storage location
            sending(str, 0);
            //calling getting message function
            getting_message();
        } catch (IOException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
    }

    /*
     * Purpose : function for getting received message
     */
    private void getting_message() {
        int r = 0;
        //system.out.println("Expected :" + str);
        String str = null;
        try {
            //reading line from serial port
            str = is.readLine();
        } catch (IOException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }

        //parsing string and getting time of received message,cell no and message
        if (str != null) {
            int l1 = str.indexOf(",");
            String cell_no = str.substring(l1 + 2, l1 + 15);
            System.out.println(cell_no);
            int l5 = str.indexOf("\"");
            l5 = str.indexOf("\"", l5 + 1);
            l5 = str.indexOf("\"", l5 + 1);
            l5 = str.indexOf("\"", l5 + 1);
            l5 = str.indexOf("\"", l5 + 1);
            int l6 = str.indexOf("\"", l5 + 1);
            String time = str.substring(l5 + 1, l6);
            System.out.println(time);
            int okindex = str.lastIndexOf("OK");
            String message = str.substring(l6 + 1, okindex);
            System.out.println(message);
            //set cell no
            crm.setCell_no(cell_no);
            //set message
            crm.setMessage(message);
            //set time
            crm.setTime(time);
        }

    }
}
