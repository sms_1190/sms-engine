/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * TestQuery.java
 *
 * Created on 19 Jun, 2010, 5:47:59 PM
 */
package sms3;

import java.sql.*;
import java.util.regex.Pattern;

/**
 *
 * @author 
 */
/**
 *
 * Class for getting result of cell query,queries for header,footer,middle messages
 */
public class TestQuery extends javax.swing.JFrame {

    /**instance of connection class*/
    Connection con = null;
    /**instance of databaseconnection.java class*/
    DatabaseConnection dc = new DatabaseConnection();

    /** Creates new form TestQuery */
    public TestQuery() {
        initComponents();
    }

    /** Function for checking query of cell numbers for any database and give result accordingly*/
    public int test_cell_query(String conn_str, String cell_query)
    {
        int result=0;
        Connection con1 = dc.db_conn(conn_str);
        
        if (con1.equals(null))
        {
            javax.swing.JOptionPane.showMessageDialog(rootPane, "Some error in connecting database.");
        } 
        else
        {
            try
            {
                Statement stmt1 = con1.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                /**executing cell number query and getting result*/
                ResultSet rs = stmt1.executeQuery(cell_query);
                ResultSetMetaData rsmd = rs.getMetaData();
                int cols = rsmd.getColumnCount();

                String output="";
                while(rs.next())
                {
                    for(int j=1;j<=cols;j++)
                    {
                        output=output+rs.getString(j)+ " ";
                    }
                    output=output+"\n";
                }

                query_result.setText(output);
                result=1;
                con1.close();
            }
            catch (SQLException e)
            {
                javax.swing.JOptionPane.showMessageDialog(null, e);
                result = 0;
            }
        }
        return result;
    }

    public int test_cell_query(String conn_str, String cell_query, String field, String unique) {
        int result = 0;
        /**Connection variable for database*/
        Connection con1 = dc.db_conn(conn_str);
        if (con1.equals(null)) {
            javax.swing.JOptionPane.showMessageDialog(rootPane, "Some error in connecting database.");
        } else {
            try {

                Statement stmt1 = con1.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                /**executing cell number query and getting result*/
                ResultSet answers1 = stmt1.executeQuery(cell_query);

                ResultSetMetaData rsmd = answers1.getMetaData();
                int cols = rsmd.getColumnCount();

                String output="";
                while(answers1.next())
                {
                    for(int j=1;j<=cols;j++)
                    {
                        output=output+answers1.getString(j)+ " ";
                    }
                    output=output+"\n";
                }

                /*
                String cell = "";
                String c, c1;
                int i = 0;
                answers1.last();
                /** getting total number of records
                i = answers1.getRow();
                answers1.beforeFirst();

                if (i <= 0) {
                    /**if no record found or query is wrong then display error message
                    cell = "No cell numbers found.";
                    result = 0;
                } else {
                    while (answers1.next()) {
                        c = answers1.getString(field);
                        /**checking for unique key, if user has entered unique key then it also display unique key value
                        if (unique.equals("")) {
                            c = c + "\n";
                        }
                        
                        else{
                            c1 = answers1.getString(unique);
                            c = c1 + "-" + c + "\n";
                        }
                         

                        cell = cell + c;
                    }
                    */

                    result = 1;
                    /**displaying result*/
                    query_result.setText(output);
                
            } catch (SQLException e) {
                javax.swing.JOptionPane.showMessageDialog(null, e);
                result = 0;
            }

        }
        return result;
    }

    /**function for checking any other query for any database and give result accordingly*/
    public int test_query(String conn_str, String conn_str_2, String cell_query, String q, String unique_key, String u_k) {
        int result = 0;

        /** retrieving the fields of query which user want,from query*/
        String q1 = q.substring(0, 6);
        int i3 = q.indexOf(q1);
        int i2 = q.indexOf("from");
        String results = null;
        String r3 = q.substring(i3 + 7, i2 - 1);
        Pattern pat = Pattern.compile(",");
        String strs[] = pat.split(r3);

        Connection con1 = dc.db_conn(conn_str);
        if (con1.equals(null)) {
            javax.swing.JOptionPane.showMessageDialog(rootPane, "Some error in connecting database.");
        } else {
            try {

                Statement stmt1 = con1.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                /**Here I am generation result for one cell number*/
                ResultSet answers1 = stmt1.executeQuery(cell_query);
                /** getting total number of records*/
                String cell = "";
                String c, c1;
                int i = 0;
                answers1.last();
                i = answers1.getRow();
                answers1.beforeFirst();

                if (i <= 0) {
                    /**if no record found then display error message*/
                    cell = "No records found.";
                    result = 0;
                } else {

                    String temp;

                    while (answers1.next()) {
                        /**retrieving unique key value from cell number query*/
                        temp = answers1.getString(unique_key);
                        Connection con2 = dc.db_conn(conn_str_2);
                        if (con2.equals(null)) {
                            javax.swing.JOptionPane.showMessageDialog(rootPane, "Some error in connecting database.");
                        } else {
                        }
                        Statement stmt2 = con2.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                                ResultSet.CONCUR_READ_ONLY);

                        if (u_k != "") {
                            /**here i am replacing unique key value of query using unique key value of cell number query
                            and also checking where clause present or not
                            if present then just add and with condition for unique key
                            else add where clause with condition of unique key
                             */
                            int i4 = q.indexOf("where");
                            if (i4 > 0) {
                                int i5 = q.indexOf(";");
                                if (i5 > 0) {
                                    q1 = q.replaceAll(";", " and " + u_k + "='" + temp + "';");

                                    System.out.println(q1);
                                } else {
                                    q1 = q + " and " + u_k + "='" + temp + "';";
                                    System.out.println(q1);
                                }
                            } else {
                                int i6 = q.indexOf(";");
                                if (i6 > 0) {
                                    q1 = q.replaceAll(";", " where " + u_k + "='" + temp + "';");

                                } else {
                                    q1 = q + " where " + u_k + "='" + temp + "';";
                                }

                            }


                        } else {
                            q1 = q;
                        }
                        cell = cell + temp;

                        ResultSet answers2 = stmt2.executeQuery(q1);

                        int j = 0, k;

                        answers2.last();
                        j = answers2.getRow();
                        answers2.beforeFirst();

                        if (j <= 0) {
                            cell = cell + "No records found.";
                            result = 0;
                        } else {
                            while (answers2.next()) {
                                /** retrieving all fields value and adding to result which is going to display*/
                                for (k = 0; k < strs.length; k++) {
                                    results = answers2.getString(strs[k]);
                                    cell = cell + "-" + results;
                                }
                            }
                        }
                        result = 1;
                        cell = cell + "\n";
                    }
                    query_result.setText(cell);
                }
            } catch (SQLException e) {
                javax.swing.JOptionPane.showMessageDialog(null, e);
                result = 0;
            }

        }


        return result;
    }

    /**function for only checking that query is syntactically right or not
    this function is used for query of keyword definition */
    public boolean testQuery(String Qry, String connstring) {
        boolean result = false;
        Connection con3 = dc.db_conn(connstring);
        if (con3 == null) {
            result = false;
        } else {
            try {
                Statement stmt3 = con3.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt3.executeQuery(Qry);
                rs.last();
                int j = rs.getRow();
                rs.beforeFirst();
                if (j == 0) {
                    result = true;
                }
            } catch (SQLException ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }

        }
        return result;
    }

    /** function for getting result of fields from query and send back*/
    public String testqu(String Qry, String connstring) {
        String result = "";
        /**retrieving fields from query*/
        String q1 = Qry.substring(0, 6);
        int i3 = Qry.indexOf(q1);
        int i2 = Qry.indexOf("from");
        String results = null;
        String r3 = Qry.substring(i3 + 7, i2 - 1);
        Pattern pat = Pattern.compile(",");
        String strs[] = pat.split(r3);
        Connection con3 = dc.db_conn(connstring);
        if (con3 == null) {
            result = "";
        } else {
            try {
                Statement stmt3 = con3.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt3.executeQuery(Qry);
                String temp, t = "";

                while (rs.next()) {
                    for (int j = 0; j < strs.length; j++) {
                        temp = rs.getString(strs[j]);
                        t = t + temp;
                    }
                }
                result = t;
                con3.close();
            } catch (SQLException ex) {
                javax.swing.JOptionPane.showMessageDialog(null, ex);
            }

        }

        return result;
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        query_result = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Query Result");

        jLabel1.setText("Selected Result :");

        query_result.setColumns(20);
        query_result.setRows(5);
        query_result.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));
        jScrollPane1.setViewportView(query_result);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 433, Short.MAX_VALUE)
                    .addComponent(jLabel1))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new TestQuery().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea query_result;
    // End of variables declaration//GEN-END:variables
}
