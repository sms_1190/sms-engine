/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimerTask;

/**
 *
 * @author 
 */
/**
 *
 * This class for sending message from queue using priority and sending it to the serial port
 */
public class SendingMessage extends TimerTask {

    private int timerInterval;
    /**creating instance of database connection class*/
    DatabaseConnection dc = new DatabaseConnection();
    /**instance of connection class*/
    Connection con;
    Statement stmt;
    ResultSet rs;
    /**instance of engine.java class*/
    Engine en;
    /**instance of checkreceivemessage.java class*/
    CheckReceiveMessage crm;
    /**instance of sender.java class*/
    Sender sender;

    SendingMessage(Engine engine, Sender send) {
        /**initialization of engine,sender,checkreceivemessage objects */
        en = engine;
        sender = send;
        crm = sender.crm;
    }
    public static String now(String dateFormat) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(cal.getTime());

    }
    public void sendingmessage(int timeInterval) {
        this.timerInterval = timeInterval;

    }

    /**function for sending message from queue according to priority*/
    public void send_message() {
        int flag = 0;
        String cell;
        String msg;
        int t;
        String q;
        int one = 1;
        try {
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }


        con = dc.database_connection_main();
        try {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            Statement stmt1 = con.createStatement();
            /**query for retrieving messages from queue according to priority
             *status is 0,message is pending
             */
            String query = "SELECT * from queue where status=0 order by priority desc;";
            rs = stmt.executeQuery(query);
            int p = 0;
            while (rs.next()) {
                /**retrieving cell no and message*/
                cell = rs.getString("cell_no");
                msg = rs.getString("message");

                t = rs.getInt("id");
                msg = msg.replace("\\n", System.getProperty("line.separator"));
                /**set status message of engine to "sending"*/
                en.status_message(cell, flag);
                /**check flag of received message*/
                int flag_from_port = crm.getFlag();
                if (flag_from_port == 1) {
                    /**if flag is set,generate message according to received message */
                    crm.make_message();
                    crm.setFlag(0);
                    /**send message to send function of sender class*/
                    flag = sender.send(cell, msg);
                } else {
                    flag = sender.send(cell, msg);
                }
                /**if message sent,update status of message*/
                if (flag == 1) {
                    en.status_message(cell, flag);
                    String date=this.now("dd-MM-yyyy");
                    String time=this.now("HH:mm");
                    q = "UPDATE queue SET status=" + one + ",sent_date='"+date+"',sent_time='"+time+"'WHERE id=" + t + "";
                    stmt1.executeUpdate(q);
                    if (p == 0) {
                        break;
                    }
                } else {
                    flag = 0;
                    continue;
                }
            }
            stmt.close();
            con.close();
            int i = 0;
            while (i <= 0) {

                con = dc.database_connection_main();
                try {
                    stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);
                    query = "SELECT * from queue where status=0 order by priority desc;";
                    rs = stmt.executeQuery(query);
                    /**checking if more message available for sending*/
                    rs.last();
                    i = rs.getRow();
                    rs.beforeFirst();

                    if (i > 0) {
                        /**if yes,call send function and get next message*/
                        send();
                    }
                    stmt.close();
                    con.close();

                } catch (SQLException ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, ex);
                }
            }
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
    }

    /**same function as above*/
    public void send() {
        con = dc.database_connection_main();
        int i = 0;
        try {
            stmt = con.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            String query = "SELECT * from queue where status=0 order by priority desc;";
            rs = stmt.executeQuery(query);

            rs.last();
            i = rs.getRow();
            rs.beforeFirst();
            if (i > 0) {
                send_message();
            } else {
                stmt.close();
                con.close();
                run();
            }

            stmt.close();
            con.close();
        } catch (SQLException ex) {
            javax.swing.JOptionPane.showMessageDialog(null, ex);
        }
    }

    @Override
    public void run() {
        send();
    }
}
