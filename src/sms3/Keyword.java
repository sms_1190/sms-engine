/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sms3;

/**
 *
 * @author 
 */
public class Keyword {

    private String keyword;//
    private String Description;//
    private String parameter[];//
    private String parameter_description[];//
    private boolean authenticate;//
    private String auth_conn;//
    private String auth_query;//
    private String paramter_missing;//
    private String query_not_satisfied;//
    private String keyword_not_matched;
    private String header_status;//
    private String footer_status;//
    private String header_query;//
    private String footer_query;//
    private String message_query;//
    private String header_conn_string;//
    private String footer_conn_string;//
    private String message_conn_string;//
    private int number_of_paramter;

    public int getNumber_of_paramter() {
        return number_of_paramter;
    }

    public void setNumber_of_paramter(int number_of_paramter) {
        this.number_of_paramter = number_of_paramter;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getAuth_conn() {
        return auth_conn;
    }

    public void setAuth_conn(String auth_conn) {
        this.auth_conn = auth_conn;
    }

    public String getAuth_query() {
        return auth_query;
    }

    public void setAuth_query(String auth_query) {
        this.auth_query = auth_query;
    }

    public boolean isAuthenticate() {
        return authenticate;
    }

    public void setAuthenticate(boolean authenticate) {
        this.authenticate = authenticate;
    }

    public String getFooter_conn_string() {
        return footer_conn_string;
    }

    public void setFooter_conn_string(String footer_conn_string) {
        this.footer_conn_string = footer_conn_string;
    }

    public String getFooter_query() {
        return footer_query;
    }

    public void setFooter_query(String footer_query) {
        this.footer_query = footer_query;
    }

    public String getFooter_status() {
        return footer_status;
    }

    public void setFooter_status(String footer_status) {
        this.footer_status = footer_status;
    }

    public String getHeader_conn_string() {
        return header_conn_string;
    }

    public void setHeader_conn_string(String header_conn_string) {
        this.header_conn_string = header_conn_string;
    }

    public String getHeader_query() {
        return header_query;
    }

    public void setHeader_query(String header_query) {
        this.header_query = header_query;
    }

    public String getHeader_status() {
        return header_status;
    }

    public void setHeader_status(String header_status) {
        this.header_status = header_status;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getKeyword_not_matched() {
        return keyword_not_matched;
    }

    public void setKeyword_not_matched(String keyword_not_matched) {
        this.keyword_not_matched = keyword_not_matched;
    }

    public String getMessage_conn_string() {
        return message_conn_string;
    }

    public void setMessage_conn_string(String message_conn_string) {
        this.message_conn_string = message_conn_string;
    }

    public String getMessage_query() {
        return message_query;
    }

    public void setMessage_query(String message_query) {
        this.message_query = message_query;
    }

    public String[] getParameter() {
        return parameter;
    }

    public void setParameter(String[] parameter) {
        this.parameter = parameter;
    }

    public String[] getParameter_description() {
        return parameter_description;
    }

    public void setParameter_description(String[] parameter_description) {
        this.parameter_description = parameter_description;
    }

    public String getParamter_missing() {
        return paramter_missing;
    }

    public void setParamter_missing(String paramter_missing) {
        this.paramter_missing = paramter_missing;
    }

    public String getQuery_not_satisfied() {
        return query_not_satisfied;
    }

    public void setQuery_not_satisfied(String query_not_satisfied) {
        this.query_not_satisfied = query_not_satisfied;
    }

    public Keyword() {
    }
}
